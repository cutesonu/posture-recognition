import os
import cv2
import numpy as np


from src.detect.yolo_utils import YoloUtils
# from src.detect.ssd_utils import SsdUtils
from src.detect.det_classify import det_classify
from utils import constant
from src.show.show_utils import ShowUtils
from src.openpose.train import load_point_classifier_model, predict, points_to_feature
from src.openpose.openpose_utils import OpenPoseUtils


classifier = load_point_classifier_model()
opu = OpenPoseUtils()
shower = ShowUtils()


class PostureClassifier:
    def __init__(self):
        # person detector
        detector = YoloUtils(model_type=constant.DET_YOLO3, score_threshold=0.0)
        # detector = SsdUtils(model_type=constant.DET_SSD, score_threshold=0.5)
        self.detector = detector

        #
        self.b_single_person = True

    def crop_dets(self, dets, img, b_only_max):
        im_h, im_w = img.shape[:2]
        if b_only_max:
            max_sz = 0
            max_det = None
            for det in dets:
                (x, y, x2, y2) = det[constant.KEY_FRECT]
                sz = (x2 - x) * (y2 - y)
                if sz > max_sz:
                    max_sz = sz
                    max_det = det
            if max_det is not None:
                dets = [max_det]

        crops = []
        for det in dets:
            (x, y, x2, y2) = (det[constant.KEY_FRECT] * np.array([im_w, im_h, im_w, im_h])).astype(np.int)
            x = max(x - constant.MARGIN, 0)
            y = max(y - constant.MARGIN, 0)
            x2 = min(x2 + constant.MARGIN, im_w)
            y2 = min(y2 + constant.MARGIN, im_h)

            crops.append({'image': img[y:y2, x:x2],
                          'score': det[constant.KEY_CONFIDENCE]})

        return crops

    def predict(self, img, b_only_max=True, b_show=True):
        show_img = img.copy()

        dets = self.detector.detect(img=img)
        crops = self.crop_dets(dets=dets, img=img, b_only_max=b_only_max)

        result = []
        for crop in crops:
            show_img = crop['image']

            # classify with using det size
            pred_lbl_1, pred_score_1, _ = det_classify(img=crop, objects=None)

            # classify with using points
            points = opu.detect(img=crop['image'])
            feature = points_to_feature(points=points)
            if feature is not None:
                pred_lbl_2, pred_score_2 = predict(classifier=classifier, feature=feature)
                show_img = opu.draw_key_points(img=show_img, points=points)
            else:
                pred_lbl_2, pred_score_2 = None, None

            if b_show:
                cv2.imshow("show", show_img)

            # combine 2 results
            if pred_lbl_1 == pred_lbl_2 or pred_lbl_2 is None:
                ret, lbl = True, pred_lbl_1
            elif pred_lbl_1 != pred_lbl_2 and pred_lbl_1 != constant.LBL_UNKNOWN:
                ret, lbl = True, pred_lbl_1
            elif pred_lbl_1 == constant.LBL_UNKNOWN and pred_lbl_2 != constant.LBL_UNKNOWN:
                ret, lbl = True, pred_lbl_2
            else:
                ret, lbl = True, constant.LBL_UNKNOWN
            result.append([ret, lbl])

        if len(result) == 0:
            result.append([False, "failed detection"])
        return result

    def do_image(self, image_path, b_show):
        image = cv2.imread(image_path)

        print("----------------------------------------------------------")
        print(image_path)

        _result = self.predict(img=image, b_only_max=True, b_show=b_show)

        if b_show:
            cv2.imshow("image", image)
            cv2.waitKey(1)
            print(_result[0])
            cv2.waitKey(1000)


if __name__ == '__main__':

    pc = PostureClassifier()

    data_folder = "data/images/all"

    for class_name in ['lying', 'sitting', 'standing']:
        folder = os.path.join(data_folder, class_name)
        paths = [os.path.join(folder, fn) for fn in os.listdir(folder) if os.path.splitext(fn)[1] in constant.IMG_EXTs]

        #
        for path in paths:
            pc.do_image(image_path=path, b_show=True)
