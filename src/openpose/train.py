import sys
import os
import numpy as np
from sklearn.svm import SVC
from sklearn.externals import joblib

from utils.constant import ROOT_DIR, MODEL_DIR, INF, LBL_SITTING, LBL_STANDING, LBL_LYING, LBL_UNKNOWN


def points_to_feature(points):
    mean_val = [0, 0]
    cnt = 0
    for val in points:
        if val[0] != INF and val[0] != INF:
            cnt += 1
            mean_val[0] += val[0]
            mean_val[1] += val[1]

    if cnt == 0:   # all points == [INF, INF]
        return None
    else:
        mean_val[0] /= cnt
        mean_val[1] /= cnt

        feature = np.array(points) - np.array(mean_val)
        feature = feature.reshape(1, -1)
        feature = feature.astype(dtype=np.int)

        return feature[0]


def load_feature_and_label():
    print('load train and labels')

    labels = []
    features = []

    categories = [LBL_SITTING, LBL_STANDING, LBL_LYING]
    # f_categories = [[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]]

    data_folder = os.path.join(ROOT_DIR, 'data/images')
    for idx, category in enumerate(categories):
        print("category: ", category)
        feature_path = os.path.join(data_folder, 'collect', category, 'feature.txt')

        # --- loading data -----------------------------------------------------------------------
        print(' loading training features ... ')
        with open(feature_path) as f:
            lines = f.readlines()

        for line in lines:
            points = []
            nodes = line.split("], [")
            for node in nodes:
                sp = node.replace(']', '').replace('[', '').replace('\n', '')
                x, y = sp.split(',')[:2]
                x = int(x)
                y = int(y)
                points.append([x, y])

            features.append(points_to_feature(points=points))
            labels.append(category)

    return features, labels, categories


def train():
    print('>>> train')
    classifier_path = os.path.join(MODEL_DIR, "classifier.pkl")

    # --- load train and label data ----------------------------------------------------------------
    features, labels, categories = load_feature_and_label()

    # --- training -----------------------------------------------------------------------------------
    print(' training... ')
    # classifier = SVC(C=1.0, kernel='linear', degree=3, gamma='auto', coef0=0.0, shrinking=True, probability=True,
    #                  tol=0.001, cache_size=200, class_weight='balanced', verbose=False, max_iter=-1,
    #                  decision_function_shape='ovr', random_state=None)
    classifier = SVC(kernel='rbf', degree=3, gamma='scale',
                     coef0=0.0, tol=1e-3, C=1.0, probability=True,
                     cache_size=100, max_iter=-1)

    classifier.fit(features, labels)
    joblib.dump(classifier, classifier_path)

    print('>>> finished the training!')


def load_point_classifier_model():
    classifier_path = os.path.join(MODEL_DIR, "classifier.pkl")

    if not os.path.exists(classifier_path):
        print(" not exist trained classifier {}".format(classifier_path))
        sys.exit(0)

    try:
        # loading
        model = joblib.load(classifier_path)
        return model
    except Exception as ex:
        print(ex)
        sys.exit(1)


def predict(classifier, feature):
    feature = feature.reshape(1, -1)
    probab = classifier.predict_proba(feature)

    max_ind = np.argmax(probab)
    sort_probab = np.sort(probab, axis=None)[::-1]  # Rearrange by size

    if sort_probab[0] > sort_probab[1] * 1.5:
        pred_lbl = classifier.classes_[max_ind]
        pred_score = sort_probab[0]
    else:
        pred_lbl = "unknown"
        pred_score = sort_probab[0]

    return pred_lbl, round(pred_score, 2)


def check_precision():
    # --- load trained classifier imgnet --------------------------------------------------------------
    print('>>> checking the precision...')
    classifier = load_point_classifier_model()

    # --- load train and label data ----------------------------------------------------------------
    features, labels, categories = load_feature_and_label()

    categories.append(LBL_UNKNOWN)
    confusion_matrix = np.zeros((len(categories), len(categories)), dtype=np.int)

    positive, negative = 0, 0
    # --- check confuse matrix ------------------------------------------------------------------------
    for i, feature in enumerate(features):
        # print(">>>GT:", labels[i])
        predlbl, score = predict(classifier=classifier, feature=feature)

        # update confusion matrix
        predlbl_idx = categories.index(predlbl)
        ground_truth_idx = categories.index(labels[i])

        confusion_matrix[predlbl_idx][ground_truth_idx] += 1

        if predlbl_idx == ground_truth_idx:
            positive += 1
            # print("\t\tPositive")
        else:
            negative += 1
            # print("\t\tNegative")
    print('>>> precision result')
    print(confusion_matrix)

    precision = positive / (positive + negative)
    print("\tprecision : {} / {} : {}%".format(positive, positive + negative, round(precision * 100, 2)))


if __name__ == '__main__':
    train()
    check_precision()
