import os
import cv2
import sys

from src.openpose.openpose_utils import OpenPoseUtils
from utils.constant import IMG_EXTs, ROOT_DIR, INF

opu = OpenPoseUtils(mode="MPI")


def collect_features(src_folder, dst_folder):
    fns = [fn for fn in os.listdir(src_folder) if os.path.splitext(fn)[1] in IMG_EXTs]
    feature_path = os.path.join(dst_folder, 'train.txt')

    if not os.path.exists(dst_folder):
        os.mkdir(dst_folder)

    lines = []
    print("src_folder:", src_folder)
    for fn in fns:
        sys.stdout.write("\t" + fn)
        src_path = os.path.join(src_folder, fn)
        dst_path = os.path.join(dst_folder, fn)

        img = cv2.imread(src_path)
        points = opu.detect(img=img)

        if len(points) == 0 or points is None:
            sys.stdout.write("\t failed\n")
            continue
        keypoint_img = opu.draw_key_points(img=img, points=points)
        cv2.imwrite(dst_path, keypoint_img)

        line = ""
        for point in points:
            if point is not None:
                x, y = point
            else:
                x, y = INF, INF
            line += "[{}, {}], ".format(x, y)
        line += "\n"
        lines.append(line)
        sys.stdout.write("\t success\n")

    with open(feature_path, 'w') as f:
        f.writelines(lines)


if __name__ == '__main__':
    data_folder = os.path.join(ROOT_DIR, 'data/images')
    categories = ["lying", "standing", "sitting"]
    for category in categories:
        _src_folder = os.path.join(data_folder, 'all', category)
        _dst_folder = os.path.join(data_folder, 'collect', category)
        collect_features(src_folder=_src_folder, dst_folder=_dst_folder)
