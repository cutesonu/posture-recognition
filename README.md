# Posture-Recognition


### Clone the project
Clone the repository from [here](https://gitlab.com/cutesonu/posture-recognition.git)

### Install the dependencies

Ubuntu 16.04
python3x

install the python packages

    pip3 install -r requirements.txt

### Download the OpenPose model

    sudo bash models/download_OpenPose_Models.sh

    root(posture-recognition)
            |
            ├── data
            |
            ├── models                        
                ├── openpose
                |   ├── face
                |   ├── hand
                |   └── pose                
                |       ├── coco
                |       └── mpi
                |
                ├── classifier.pkl                      # classifier model
                └── download_OpenPose_Models.sh         # bash script for douwnloading the openpose model
            ...


### Test with trained classifier model

    python3 main.py sample.jpg
    python3 main.py sample.avi

### Train with new train data
    
- Prepare the train data with following structure



        root(posture-recognition)
                |
                ├── data                        
                    └── images                     # train data
                        ├── all                    # raw data                
                        |    ├── lying
                        |    ├── sitting
                        |    └── standing
                        └── collect
                ...

- Collect the features
  

        python3 src/train/collect_features.py

- Training


        python3 src/train/train.py


### Result
344

failed 	:	40
standing:	167
lying	: 	118
sitting	:	15
unknown	:	4
